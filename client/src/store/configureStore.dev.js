import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import DevTools from '../util/DevTools';
import { history } from '../index';
// import { loadState } from './localStorage';
// import { saveState } from './localStorage';

const configureStore = preloadedState => {

  // const persistedState = loadState();

  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk, createLogger(), routerMiddleware(history)),
      DevTools.instrument()
    )
  )

  // store.subscribe(() => {
  //   saveState(store.getState());
  // })

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    })
  }
  return store
}

export default configureStore
