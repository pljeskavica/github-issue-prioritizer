import React from 'react'
import registerServiceWorker from './registerServiceWorker';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { Route, Link, Switch } from 'react-router-dom'
import Routes from './routes'
import createHistory from 'history/createBrowserHistory';
import configureStore from './store';
import Header from './Components/header';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

// import './css/App.css';
// import './css/font-awesome.min.css';

const target = document.querySelector('#root')

export const history = createHistory();
const store = configureStore();

injectTapEventPlugin();
console.log(store)
console.log(history)

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <MuiThemeProvider>
      <div>
         <div className="app">
          <Header />
        {Routes}
      </div>
      </div>
      </MuiThemeProvider>
    </ConnectedRouter>
  </Provider>,
  target
)

registerServiceWorker();
 