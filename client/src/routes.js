import React from 'react';
import { Route, Switch } from 'react-router';

//Import Containers
import LoginContainer from './Containers/LoginContainer';
import RepoContainer from './Containers/RepoContainer';
import IssueContainer from './Containers/IssueContainer';


const routes = (
  <div>
    <Route/>
    <Switch>
      <Route path="/" render={(props) => (
          <LoginContainer {...props} />
      )} />
      <Route path="/login" render={(props) => (
          <LoginContainer {...props} />
      )} />
      <Route path="/repos" render={(props) => (
          <RepoContainer {...props} />
      )} />
      <Route path="/issues" render={(props) => (
          <IssueContainer {...props} />
      )} />
    </Switch>
  </div>
)

export default routes
