import request from 'supertest';
import app from '../app';

describe('Health Checks', () => {
    test('It should give a 200 Response and and OK', () => {
        return request(app).get("/ping").then(response => {
            expect(response.text).toBe('OK')
            expect(response.statusCode).toBe(200)
        })
    });
})