import {Router} from 'express';
import * as healthController from '../Controllers/healthCheck.controller';

const router = new Router;

router.route('/')
  .get(healthController.resolveHealthCheck);

export default router;