import app from './app';
// Import Config
import * as Config from './config';

// Make server changes and route additions in app.js
// Seperated to support tests using supertest.js

app.listen(Config.port,  () => {
  console.log(`Server listening on port ${Config.port}!`)
});

