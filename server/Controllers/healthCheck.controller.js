export const resolveHealthCheck = (req, res, next) => {
  return res.end('OK');
}