import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

// Import Routes
import healthCheckRoutes from './Routes/healthCheck.routes';


const app = express();


app.use(express.static(path.join(__dirname, '/../public/')));
app.use(bodyParser.json());


// Routes
app.use('/ping', healthCheckRoutes);

export default app;