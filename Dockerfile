FROM node:8.6.0


WORKDIR /opt/app

RUN mkdir -p /opt/app/client

# Copy the Server package docs over
ADD package.json package-lock.json /opt/app/
ADD package.json package-lock.json /tmp/
# Client Package Docs
RUN mkdir -p /tmp/client
ADD client/package.json /tmp/client
ADD client/package.json /opt/app/client


# Install packages to tmp and link them to the app folder
RUN cd /tmp && npm run install-all
RUN cd /opt/app && ln -s /tmp/node_modules
RUN cd /opt/app/client && ln -s /tmp/client/node_modules && cd ..

# # Copy the code
ADD . /opt/app/
# Build the app
RUN npm run build

EXPOSE 8000

CMD ["npm" , "start"]
